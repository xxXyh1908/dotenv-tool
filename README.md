<h1 align="center">Welcome to dotenv-tool 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <img src="https://img.shields.io/badge/node-%3E%3D12.0.0-blue.svg" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
</p>

> base Generate dotenv variables, support js,json,yaml
### 🏠 [Homepage](https://gitee.com/xxXyh1908/dotenv-tool/#readme)

## Prerequisites

- node >=12.0.0

## Usage

### generate dts files

```bash
dotenv-tool types/env.d.ts
```

## 🤝 Contributing

Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://gitee.com/xxXyh1908/dotenv-tool/issues).

## Show your support

Give a ⭐️ if this project helped you!

***
_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_