"use strict";Object.defineProperty(exports, "__esModule", {value: true}); function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }




var _chunkBYFGBJAPjs = require('./chunk-BYFGBJAP.js');

// src/rollup-plugin.ts
var _rollupplugindefine = require('rollup-plugin-define'); var _rollupplugindefine2 = _interopRequireDefault(_rollupplugindefine);
function dotenv(options = {}) {
  options.outputDts = options.outputDts || "src/typings/env.d.ts";
  options.dtsTarget = options.dtsTarget || ["global", "process.env", "import.meta.env"];
  const { include, exclude } = options;
  const env = _chunkBYFGBJAPjs.parse.call(void 0, options);
  _chunkBYFGBJAPjs.generateDts.call(void 0, env, options);
  const defines = _chunkBYFGBJAPjs.pickBy_default.call(void 0, _chunkBYFGBJAPjs.generateDefines.call(void 0, env), (_, key) => !key.includes("["));
  const replacements = {};
  for (const target of options.dtsTarget) {
    switch (target) {
      case "global":
        Object.assign(replacements, defines);
        break;
      default:
        Object.assign(replacements, Object.fromEntries(Object.entries(defines).map((key, value) => [`${target}.${key}`, value])));
        break;
    }
  }
  return _rollupplugindefine2.default.call(void 0, {
    include,
    exclude,
    replacements
  });
}


exports.default = dotenv;
