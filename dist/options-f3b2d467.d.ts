interface Options {
    /**
     * Env file prefix
     * The parser will parse files such as [.env,.env.js,.env.json].
     * @default .env
     */
    prefix?: string;
    /**
     * enabled process.env.NODE_ENV
     * @default true
     */
    enabledMode?: boolean;
    /**
     * @default process.env.NODE_ENV
     */
    mode?: string;
    /**
     * output dts file
     * @default false
     */
    outputDts?: false | string;
    /**
     * support 'global', 'process.env', 'import.meta.env'
     * @default ['process.env']
     */
    dtsTarget?: string[];
}
declare function resolveOptions(options?: Options): Required<Options>;

export { Options as O, resolveOptions as r };
