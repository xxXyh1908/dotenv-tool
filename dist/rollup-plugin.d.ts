import * as rollup from 'rollup';
import { O as Options } from './options-f3b2d467';
import { FilterPattern } from '@rollup/pluginutils';

interface PluginOptions extends Options {
    include?: FilterPattern;
    exclude?: FilterPattern;
    /**
     * @default 'src/typings/env.d.ts'
     */
    outputDts?: string;
    /**
     * support 'global', 'process.env', 'import.meta.env'
     * @default ['global','process.env','import.meta.env']
     */
    dtsTarget?: string[];
}
declare function dotenv(options?: PluginOptions): rollup.Plugin;

export { PluginOptions, dotenv as default };
