import { O as Options } from './options-f3b2d467';
export { O as Options, r as resolveOptions } from './options-f3b2d467';

declare function generateDts(options?: Options): void;
declare function generateDts(obj: Record<string, any>, options: Options): void;
declare function generateDefines(obj: Record<string, any>): Record<string, any>;

declare function parse(options?: Options): Record<string, any>;

declare function stringifyTyped(data: any): string;
declare function stringifyValue(data: any): string;

export { generateDefines, generateDts, parse, stringifyTyped, stringifyValue };
