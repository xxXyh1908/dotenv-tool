import { O as Options } from './options-f3b2d467';
import { DefinePlugin } from 'webpack';

interface PluginOptions extends Options {
    /**
     * @default 'src/typings/env.d.ts'
     */
    outputDts?: string;
    /**
     * support 'global', 'process.env', 'import.meta.env'
     * @default ['global','process.env','import.meta.env']
     */
    dtsTarget?: string[];
}
declare class Dotenv extends DefinePlugin {
    constructor(options?: PluginOptions);
}

export { PluginOptions, Dotenv as default };
