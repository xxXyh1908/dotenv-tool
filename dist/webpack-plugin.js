"use strict";Object.defineProperty(exports, "__esModule", {value: true});




var _chunkBYFGBJAPjs = require('./chunk-BYFGBJAP.js');

// src/webpack-plugin.ts
var _webpack = require('webpack');
var Dotenv = class extends _webpack.DefinePlugin {
  constructor(options = {}) {
    options.outputDts = options.outputDts || "src/typings/env.d.ts";
    options.dtsTarget = options.dtsTarget || ["global", "process.env", "import.meta.env"];
    const env = _chunkBYFGBJAPjs.parse.call(void 0, options);
    _chunkBYFGBJAPjs.generateDts.call(void 0, env, options);
    const defines = _chunkBYFGBJAPjs.pickBy_default.call(void 0, _chunkBYFGBJAPjs.generateDefines.call(void 0, env), (_, key) => !key.includes("["));
    const replacements = {};
    for (const target of options.dtsTarget) {
      switch (target) {
        case "global":
          Object.assign(replacements, defines);
          break;
        default:
          Object.assign(replacements, Object.fromEntries(Object.entries(defines).map((key, value) => [`${target}.${key}`, value])));
          break;
      }
    }
    super(replacements);
  }
};


exports.default = Dotenv;
