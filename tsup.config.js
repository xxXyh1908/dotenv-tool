const pkg = require('./package.json')

module.exports = {
  clean: true,
  dts: true,
  outDir: 'dist',
  external: [...Object.keys(pkg.dependencies ?? {}),...Object.keys(pkg.peerDependencies ?? {})],
  format: ['cjs'],
  entryPoints: ['src/index.ts', 'src/rollup-plugin.ts', 'src/webpack-plugin.ts'],
}
