export interface Options {
  /**
   * Env file prefix
   * The parser will parse files such as [.env,.env.js,.env.json].
   * @default .env
   */
  prefix?: string

  /**
   * enabled process.env.NODE_ENV
   * @default true
   */
  enabledMode?: boolean

  /**
   * @default process.env.NODE_ENV
   */
  mode?: string

  /**
   * output dts file
   * @default false
   */
  outputDts?: false | string

  /**
   * support 'global', 'process.env', 'import.meta.env'
   * @default ['process.env']
   */
  dtsTarget?: string[]
}

export function resolveOptions(options: Options = {}): Required<Options> {
  return {
    prefix: options.prefix || '.env',
    enabledMode: options.enabledMode ?? true,
    mode: options.mode || process.env.NODE_ENV || '',
    outputDts: options.outputDts || false,
    dtsTarget: options.dtsTarget ?? ['process.env']
  }
}
