import { Command } from 'commander'
import { generateDts, Options } from '.'
import pkg from '../package.json'

const program = new Command()

program
  .version(pkg.version)
  .description(pkg.description)
  .arguments('<dts-path>')
  .option('-p, --prefix <path>', 'Env file prefix', '.env')
  .option('-m, --mode <mode>', 'Application mode', process.env.NODE_ENV)
  .option('--disabled-mode', 'Disabled application mode', false)
  .option(
    '-t, --target <targets>',
    `support 'global', 'process.env', 'import.meta.env'`,
    'process.env'
  )
  .helpOption()
  .action((dtsPath: string, options: any) => {
    const opts: Options = {
      prefix: options.prefix,
      mode: options.mode,
      enabledMode: true,
      outputDts: dtsPath,
      dtsTarget: options.target.split(','),
    }

    generateDts(opts)
  })
  .parse(process.argv)
